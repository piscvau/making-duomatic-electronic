//Ajout de la librairie pour le servo moteur AX12
#include <ax12.h>

//Ajout de la librairie pour le moteur pas à pas
#include <Stepper.h>

// Definition de notre servo moteur (avec son adresse, ici 1)
AX12 servomoteur(1); 

// Nombre de pas par tour du moteur (ici 200)
const int stepsPerRevolution = 200;

// Initialisation de la librairie "stepper" avec la carte motor shield
Stepper myStepper(stepsPerRevolution, 12,13);     

// Definition des entrees pour le moteur 
const int pwmA = 3;
const int pwmB = 11;
const int brakeA = 9;
const int brakeB = 8;
const int dirA = 12;
const int dirB = 13;


void setup()
{
  
// Vitesse réglée en usine, 1Mb/s.
   AX12::init(1000000); 
   
   //Parametrage servo
   servomoteur.writeInfo(TORQUE_ENABLE, 1);
   servomoteur.writeInfo(MAX_TORQUE, 500);

   //moteur.setEndlessTurnMode(true);
   //moteur.endlessTurn(500);

// Definition des différents variables pwm et brake en sortie et leurs états
pinMode(pwmA, OUTPUT);
pinMode(pwmB, OUTPUT);
pinMode(brakeA, OUTPUT);
pinMode(brakeB, OUTPUT);
digitalWrite(pwmA, HIGH);
digitalWrite(pwmB, HIGH);
digitalWrite(brakeA, LOW);
digitalWrite(brakeB, LOW);

//Parametrage vitesse moteur :
myStepper.setSpeed(30);   

//On commence par lever la partie bascule
myStepper.step(50);
delay (2000);
}

// Temps
int t = 0; 
// Direction du déplacement
int dir = 1; 

void loop()
{
  //changement de sens lorsque les 300 degrés sont atteints
   t += dir;
   if(t <= -150 || t >= 150)
   {
     dir = -dir;
   }
   
   //Position initiale : descente de la partie bascule
   //Puis attente de 3 secondes 
   if(t==150) 
   {
   myStepper.step(-50);
   delay (3000);
   
   }
   
   //Attente de 5 secondes lorsque position 0 degré
   if(t==0)
   {
   delay (5000);
   } 
   
   //Attente de 5 secondes lorsque position 90 degrés
   if(t==-90) 
   {
   delay (5000);
   }
   
   //Attente de 5 secondes lorsque position 120 degrés
   if(t==-120)
   {
   delay (5000);
   }
   
   //Position finale atteinte : Monter de la partie bascule
   //Puis Attente 
   if(t==-150)
   {
   myStepper.step(-50);
   delay (1000000);
   }

//Appelle de la fonction angle
   angle(servomoteur, t);

   delay(10);
}

//Fonction angle :
//Entrees : servomoteur et angle
//Sortie : renvoie une position angulaire au servomoteur
byte angle(AX12 ax, int a)
{
  // On fait varier a entre -150 et 150 (corrrespondant au degré)
   return ax.writeInfo(GOAL_POSITION, map(a, -150, 150, 0, 1023));
}
